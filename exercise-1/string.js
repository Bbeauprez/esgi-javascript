function ucfirst(str) {
    return typeof str === 'string' ? str[0].toUpperCase() + str.slice(1) : "";
}

function capitalize(str) {
    if (typeof str !== 'string') return "";
    str = str.toLowerCase().split(' ');
    const res = str.map(word => {
        if (word !== undefined)
            return ucfirst(word);
    });

    return res.join(' ');
}

function camelCase(str) {
    if (typeof str !== 'string') return "";
    return capitalize(str).replace(/[^a-z0-9]/g, '');
}

function snake_case(str) {
    if (typeof str !== 'string') return "";
    return str.toLowerCase().replace(/[^a-z0-9]/g, '_');
}

function leet(str) {
    if (typeof str !== 'string') return "";
    return str.replace(/a/gi, '4').replace(/e/gi, '3').replace(/i/gi, '1')
        .replace(/o/gi, '0').replace(/u/gi, '(_)').replace(/y/gi, '7');
}

function prop_access(array, str) {
    if (typeof str !== 'string' || array === null || str === '') return array;
    const index = str.split('.');
    for (let i = 0; i < index.length; i++) {
        if (array.hasOwnProperty(index[i])) {
            if (i < index.length - 1)
                array = array[index[i]];
            else
                return array[index[i]];
        } else {
            let string = '';
            for (let y = 0; y <= i; y++) {
                string += '.' + index[y];
            }
            return string.replace('.', '') + ' not exist';
        }
    }
}

function verlan(str) {
    return str.split("").reverse().join("").split(" ").reverse().join(" ")
}

function yoda(str) {
    return str.split(' ').reverse().join(' ');
}

function vig(str, key) {
    if (typeof str !== 'string') return "";
    while (key.length < str.length)
        key += key;
    return str.split('').map((char, index) => {
        const charCode = char.charCodeAt(0) - "a".charCodeAt(0);
        const keyCode = key[index].charCodeAt(0) - "a".charCodeAt(0);

    })
}