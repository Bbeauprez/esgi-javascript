function type_check_v1(evoli, type) {
    if (typeof type !== 'string' ) return false;
    if (typeof evoli === "object") {
        if (Array.isArray(evoli)) return type === 'array';
        if (evoli === null) return type === 'null';
        return typeof evoli === type;
    } else {
        return typeof evoli === type;
    }
}

function type_check_v2(evoli, obj) {
    if (typeof obj !== 'object' || obj === null || obj === undefined) return false;
    if (!'enum' in obj && !'type' in obj && !'value' in obj) return false;
    let testEnum = true;
    let testType = true;
    let testVal = true;

    if ('enum' in obj)
        testEnum = obj.enum.includes(evoli);
    if ('type' in obj)
        testType = type_check_v1(evoli, obj.type);
    if ('value' in obj)
        testVal = evoli === obj.value;

    return testEnum && testType && testVal;
}


console.log(type_check_v2('test', {type: "string"}));